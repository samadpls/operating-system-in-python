import tkinter as tk
from tkinter import filedialog, messagebox, simpledialog, scrolledtext
from tkinter import ttk
import subprocess
import threading
import os


class CustomOS:
    def __init__(self, root):
        self.root = root
        self.root.title("Custom OS")
        self.root.geometry("600x400")  # Set window size

        # Change window background color
        self.root.configure(bg="#5C4033")  # Royal Blue color

        # Create a menu bar
        self.menu_bar = tk.Menu(root)
        self.root.config(menu=self.menu_bar)

        # File menu
        self.file_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.file_menu.add_command(
            label="Create Folder", command=self.create_folder)
        self.file_menu.add_command(
            label="Create File", command=self.create_file)
        self.file_menu.add_command(
            label="Change File Rights", command=self.change_rights
        )
        self.file_menu.add_separator()
        self.file_menu.add_command(
            label="Search Files", command=self.search_files)
        self.menu_bar.add_cascade(label="File", menu=self.file_menu)

        # Process menu
        self.process_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.process_menu.add_command(
            label="Sort Array (Process)", command=self.sort_array_process
        )
        self.process_menu.add_command(
            label="Matrix Operations (Threads)", command=self.matrix_operations_thread
        )
        self.process_menu.add_separator()
        self.process_menu.add_command(
            label="Task Manager", command=self.task_manager)
        self.menu_bar.add_cascade(label="Processes", menu=self.process_menu)

        # Application menu
        self.app_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.app_menu.add_command(
            label="Open Firefox🦊", command=lambda: self.launch_application("firefox"
                                                                           ))
        self.app_menu.add_command(
            label="Open Image Viewer",
            command=lambda: self.launch_application("image_viewer"),
        )
        self.menu_bar.add_cascade(label="Applications", menu=self.app_menu)

        # Add emojis or pictures
        emoji_label = tk.Label(
            root, text="Hello\n @samadpls,\n @Shaheer \n  @Taha", font=("Arial", 30), bg="#5C4033", fg="white"
        )
        emoji_label.pack(pady=20)
        self.data_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.data_menu.add_command(
            label="Share Data (Process1)", command=self.share_data_process1)
        self.data_menu.add_command(
            label="Share Data (Process2)", command=self.share_data_process2)
        self.menu_bar.add_cascade(label="Data Sharing", menu=self.data_menu)
        self.custom_programs_menu = tk.Menu(self.process_menu, tearoff=0)
        self.custom_programs_menu.add_command(
            label="Write Program", command=self.write_program)
        self.custom_programs_menu.add_command(
            label="Execute Program", command=self.execute_program)
        self.custom_programs_menu.add_command(
            label="Delete Program", command=self.delete_program)
        self.process_menu.add_cascade(
            label="Custom Python Programs", menu=self.custom_programs_menu)

        # Run the main loop
        root.mainloop()

    def create_folder(self):
        folder_name = filedialog.askdirectory(title="Create Folder")
        if folder_name:
            try:
                os.mkdir(folder_name)
                messagebox.showinfo(
                    "Success", f"Folder '{folder_name}' created successfully."
                )
            except Exception as e:
                messagebox.showerror(
                    "Error", f"Failed to create folder. Error: {e}")

    def create_file(self):
        file_name = filedialog.asksaveasfilename(
            title="Create File",
            defaultextension=".txt",
            filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")],
        )
        if file_name:
            try:
                with open(file_name, "w") as file:
                    file.write(" ")
                messagebox.showinfo(
                    "Success", f"File '{file_name}' created successfully."
                )
            except Exception as e:
                messagebox.showerror(
                    "Error", f"Failed to create file. Error: {e}")

    def write_program(self):
        program_name = simpledialog.askstring(
            title="Write Python Program", prompt="Enter program name:")
        if program_name:
            try:
                program_code = self.get_multiline_input(
                    "Write Python Program", "Enter Python code:")
                if program_code:
                    program_file_path = f"{program_name}.py"
                    with open(program_file_path, 'w') as program_file:
                        program_file.write(program_code)
                    messagebox.showinfo(
                        "Custom Python Programs", f"Program '{program_name}' written successfully.")
                else:
                    messagebox.showwarning(
                        "Custom Python Programs", "Program code cannot be empty.")
            except Exception as e:
                messagebox.showerror(
                    "Error", f"Failed to write program. Error: {e}")

    def get_multiline_input(self, title, prompt):
        input_dialog = tk.Toplevel(self.root)
        input_dialog.title(title)

        label = tk.Label(input_dialog, text=prompt, font=("Arial", 12))
        label.pack(padx=10, pady=10)

        input_text = scrolledtext.ScrolledText(
            input_dialog, width=60, height=20, wrap=tk.WORD)
        input_text.pack(padx=10, pady=10)

        def on_ok():
            input_dialog.result = input_text.get("1.0", tk.END).strip()
            input_dialog.destroy()

        ok_button = ttk.Button(input_dialog, text="OK", command=on_ok)
        ok_button.pack(pady=10)

        input_dialog.geometry("600x500")
        input_dialog.transient(self.root)
        input_dialog.grab_set()
        self.root.wait_window(input_dialog)

        return input_dialog.result

    def execute_program(self):
        program_name = simpledialog.askstring(
            title="Execute Python Program", prompt="Enter program name:")
        if program_name:
            program_file_path = f"{program_name}.py"
            if os.path.exists(program_file_path):
                try:
                    output = subprocess.check_output(
                        ["python3", program_file_path]).decode()
                    messagebox.showinfo(
                        "Custom Python Programs", f"Execution Output:\n{output}")
                except Exception as e:
                    messagebox.showerror(
                        "Error", f"Failed to execute program. Error: {e}")
            else:
                messagebox.showwarning(
                    "Custom Python Programs", f"Program '{program_name}' not found.")

    def delete_program(self):
        program_name = simpledialog.askstring(
            title="Delete Python Program", prompt="Enter program name:")
        if program_name:
            program_file_path = f"{program_name}.py"
            if os.path.exists(program_file_path):
                try:
                    os.remove(program_file_path)
                    messagebox.showinfo(
                        "Custom Python Programs", f"Program '{program_name}' deleted successfully.")
                except Exception as e:
                    messagebox.showerror(
                        "Error", f"Failed to delete program. Error: {e}")
            else:
                messagebox.showwarning(
                    "Custom Python Programs", f"Program '{program_name}' not found.")

    def sort_array_process(self):
        array = [4, 2, 8, 1, 6]
        sorted_array = sorted(array)
        messagebox.showinfo(
            "Sort Array (Process)",
            f"Original Array: {array}\nSorted Array: {sorted_array}",
        )

    def change_rights(self, item_path=None):
        if not item_path:
            item_path = filedialog.askopenfilename(title="Select File or Folder")

        if item_path:
            try:
                permission_value = simpledialog.askinteger(
                    "Change Rights",
                    f"Enter permission value for '{item_path}':",
                    initialvalue=0o777
                )

                if permission_value is not None:
                    os.chmod(item_path, permission_value)
                    print(f"Changed rights for '{item_path}'.")
            except Exception as e:
                print(f"Error changing rights: {e}")

    def search_files(self):
        search_term = simpledialog.askstring(
            title="Search Files", prompt="Enter search term:"
        )
        if search_term:
            result_message = f"File selected: {search_term}"
            threading.Thread(
                target=self._search_files_thread, args=(search_term,)
            ).start()

    def _search_files_thread(self, search_term):
        try:
            search_term = search_term.lower()
            search_results = []
            start_dir = "."  # Set to the current directory
            for root, _, files in os.walk(start_dir):
                for file in files:
                    if search_term in file.lower():
                        search_results.append(os.path.join(root, file))
            if search_results:
                messagebox.showinfo(
                    "Search Results", "\n".join(search_results))
            else:
                messagebox.showinfo("Search Results", "No files found.")
        except PermissionError:
            messagebox.showerror("Error", "Permission denied.")
        except Exception as e:
            messagebox.showerror(
                "Error", f"Failed to search files. Error: {e}")

    def matrix_operations_thread(self):
        def matrix_operation():
            matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
            transpose = [
                [matrix[j][i] for j in range(len(matrix))]
                for i in range(len(matrix[0]))
            ]
            messagebox.showinfo(
                "Matrix Operations (Threads)",
                f"Original Matrix:\n{matrix}\n\nTranspose Matrix:\n{transpose}",
            )

        thread = threading.Thread(target=matrix_operation)
        thread.start()

    def task_manager(self):
        try:
            output = subprocess.check_output(["ps", "-ef"]).decode()
            messagebox.showinfo("Task Manager", output)
        except Exception as e:
            messagebox.showerror(
                "Error", f"Failed to get process list. Error: {e}")

    def share_data_process1(self):
        # Process 1: Writes data to a shared file
        data = simpledialog.askstring(
            title="Data Sharing", prompt="Enter data to share:"
        )
        if data:
            try:
                with open("shared_data.txt", "w") as file:
                    file.write(data)
                messagebox.showinfo(
                    "Data Sharing", f"Data shared successfully by Process 1."
                )
            except Exception as e:
                messagebox.showerror(
                    "Error", f"Failed to share data. Error: {e}")

    def share_data_process2(self):
        # Process 2: Reads data from the shared file
        try:
            with open("shared_data.txt", "r") as file:
                shared_data = file.read()
            messagebox.showinfo(
                "Data Sharing", f"Data received by Process 2: {shared_data}"
            )
        except FileNotFoundError:
            messagebox.showinfo(
                "Data Sharing",
                "No shared data found. Process 1 might not have shared data yet.",
            )
        except Exception as e:
            messagebox.showerror(
                "Error", f"Failed to read shared data. Error: {e}")

    def launch_application(self, app_name):
        try:
            if app_name == "firefox":
                subprocess.run(["firefox"])
            elif app_name == "image_viewer":
                subprocess.run(["xdg-open"])
            # Add more applications as needed
        except Exception as e:
            messagebox.showerror(
                "Error", f"Failed to launch application. Error: {e}")


if __name__ == "__main__":
    # Create the main window
    root = tk.Tk()
    style = ttk.Style()
    style.configure("TButton", font=("Arial", 12, "bold"), foreground="blue")
    style.configure("TLabel", font=("Arial", 14), foreground="green")
    custom_os = CustomOS(root)
