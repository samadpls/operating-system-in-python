
# Custom OS Project (app.py)

The **Custom OS** project, implemented in `app.py`, is a simple file management and utility application built using the Python `tkinter` library. It provides a graphical user interface (GUI) for performing various file operations, managing custom Python programs, and executing system processes.

## Features

- **File Operations:**
  - Create Folders
  - Create Files
  - Change File Rights (Permissions)
  - Search Files

- **Processes:**
  - Sort Array (Process)
  - Matrix Operations (Threads)
  - Task Manager

- **Applications:**
  - Open Firefox
  - Open Image Viewer

- **Data Sharing:**
  - Share Data (Process 1)
  - Share Data (Process 2)

- **Custom Python Programs:**
  - Write Python Programs
  - Execute Python Programs
  - Delete Python Programs

## How to Use

1. **File Operations:**
   - Click on the "File" menu to access file-related operations.

2. **Processes:**
   - Explore the "Processes" menu for array sorting, matrix operations, and task management.

3. **Applications:**
   - Launch Firefox or an Image Viewer using the "Applications" menu.

4. **Data Sharing:**
   - Share data between processes using the "Data Sharing" menu.

5. **Custom Python Programs:**
   - Write, execute, and delete custom Python programs.

## Requirements

- Python 3.x
- `tkinter` library

## Usage

```bash
python app.py
```

## Note

- The application uses the `tkinter` library for the graphical user interface.
- Additional customization and functionality can be added to suit specific needs.
